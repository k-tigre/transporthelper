package by.tigre.transtorthelper.core;

/**
 * Created by tigre on 25.03.15.
 */
public abstract class Constants {
    //preference keys
    public static final String KEY_SETTINGS = "settings";
    public static final String KEY_LAST_UPDATE = "last_update";

    //intent actions
    public static final String ACTION_CMD_SYNC = "action_sync";
    public static final String ACTION_CMD_STATE = "action_state";
    public static final String ACTION_STATE = "action_state";
    public static final String ACTION_ERROR = "action_error";
    public static final String ACTION_TRANSTORT = "action_transport";

    //intent keys
    public static final String KEY_FORCE = "key_force";
    public static final String KEY_PROGRESS = "key_progress";
}
