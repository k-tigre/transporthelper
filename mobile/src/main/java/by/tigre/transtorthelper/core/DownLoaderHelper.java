package by.tigre.transtorthelper.core;

import android.content.Context;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;


public class DownLoaderHelper {
    protected final String link;
    protected final String file;
    protected onDownloaderCallBask listener;
    protected String date;
    protected final int type;
	public DownLoaderHelper(String link, String file, String date,
                            onDownloaderCallBask listener, int type) {
		super();
        Log.i("DownLoaderDB", "file = " + link);
		this.link = link;
		this.file = file;
		this.listener = listener;
		this.date = date;
        this.type = type;
    }

	private String error;



	public static boolean downloadHttpTr(Context context, String link, String dbPath) {
		boolean result = true;
        String error = null;
		Log.i("UpdateService", "down start = DB");
		HttpClient httpclient = new DefaultHttpClient();
		HttpParams params = httpclient.getParams();
		int timeoutConnection = 3*60 * 1000;
		HttpConnectionParams.setConnectionTimeout(params, timeoutConnection);
		int timeoutSocket = 15 * 1000;
		HttpConnectionParams.setSoTimeout(params, timeoutSocket);
		HttpGet httpget = new HttpGet(link);

			HttpResponse response;
        InputStream instream = null;
        File file = new File(dbPath);
			try {
				response = httpclient.execute(httpget);
				System.out.println(response.getStatusLine());
				HttpEntity entity = response.getEntity();
				if (entity != null) {
					instream = entity.getContent();
					try {
						file.mkdirs();
						if (ZIP.UnPackZIP(instream, file.getAbsolutePath()))
							error = null;
						else
							error = "unzipError";
					} catch (RuntimeException ex) {
						ex.printStackTrace();
						httpget.abort();
						result = false;
						error = ex.toString();
					} finally {
						try {
							if (instream != null)
								instream.close();
						} catch (Exception e) {
						}
					}
				} else {
					error = "no data";
				}
				response.getEntity().consumeContent();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
				result = false;
				error = e.toString();
			} catch (IOException e) {
				e.printStackTrace();
				result = false;
				error = e.toString();
			} finally {
                if (instream!=null){
                    try {
                        instream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
			}
		Log.i("UpdateService", "down finish DB");
		return result;
	}
	


	public interface onDownloaderCallBask {
		void onLoadOk(String date, int type);

		void onLoadError(String error, int type);
	}

}
