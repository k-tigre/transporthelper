package by.tigre.transtorthelper.core;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;


public class Utils {
    public static final SimpleDateFormat DATE_FORMAT_TITLE = new SimpleDateFormat("dd MMMM yyyy");
    public static final SimpleDateFormat TIME_DATE_FORMAT = new SimpleDateFormat("HH:mm:ss  dd MMMM yyyy");


    public static String transliterate(String srcstring) {
        if (srcstring == null || srcstring.length() == 0) {
            return "";
        }
        ArrayList<String> copyTo = new ArrayList<String>();

        String cyrcodes = "";
        for (int i = 1040; i <= 1103; i++) {
            cyrcodes = cyrcodes + (char) i;
        }
        // for (int j = 1072; j <= 1103; j++) {
        // cyrcodes = cyrcodes + (char) j;
        // }
        // Uppercase
        copyTo.add("A");
        copyTo.add("B");
        copyTo.add("V");
        copyTo.add("G");
        copyTo.add("D");
        copyTo.add("E");
        copyTo.add("Zh");
        copyTo.add("Z");
        copyTo.add("I");
        copyTo.add("I");
        copyTo.add("K");
        copyTo.add("L");
        copyTo.add("M");
        copyTo.add("N");
        copyTo.add("O");
        copyTo.add("P");
        copyTo.add("R");
        copyTo.add("S");
        copyTo.add("T");
        copyTo.add("U");
        copyTo.add("F");
        copyTo.add("Kh");
        copyTo.add("TS");
        copyTo.add("Ch");
        copyTo.add("Sh");
        copyTo.add("Shch");
        copyTo.add("");
        copyTo.add("Y");
        copyTo.add("'");
        copyTo.add("E");
        copyTo.add("Yu");
        copyTo.add("Ya");

        // lowercase
        copyTo.add("a");
        copyTo.add("b");
        copyTo.add("v");
        copyTo.add("g");
        copyTo.add("d");
        copyTo.add("e");
        copyTo.add("zh");
        copyTo.add("z");
        copyTo.add("i");
        copyTo.add("i");
        copyTo.add("k");
        copyTo.add("l");
        copyTo.add("m");
        copyTo.add("n");
        copyTo.add("o");
        copyTo.add("p");
        copyTo.add("r");
        copyTo.add("s");
        copyTo.add("t");
        copyTo.add("u");
        copyTo.add("f");
        copyTo.add("kh");
        copyTo.add("ts");
        copyTo.add("ch");
        copyTo.add("sh");
        copyTo.add("shch");
        copyTo.add("");
        copyTo.add("y");
        copyTo.add("'");
        copyTo.add("e");
        copyTo.add("yu");
        copyTo.add("ya");

        String newstring = "";
        char onechar;
        int replacewith;
        for (int j = 0; j < srcstring.length(); j++) {
            onechar = srcstring.charAt(j);
            replacewith = cyrcodes.indexOf((int) onechar);
            if (replacewith > -1) {
                newstring = newstring + copyTo.get(replacewith);
            } else {
                // keep the original character, not in replace list
                newstring = newstring + String.valueOf(onechar);
            }
        }

        return newstring;
    }

    public static boolean checkNearMinsk(Location location) {
//        • 54.025, 27.25
//        • 53.73, 27.25
//        • 54.025, 28.1
//        • 53.73, 28.1
        return location != null && location.getLatitude() < 54.25 && location.getLatitude() > 53.73
                && location.getLongitude() > 27.25 && location.getLongitude() < 28.1;
    }

    public static boolean checkNearMinsk(double latitude, double longitude) {
//        • 54.025, 27.25
//        • 53.73, 27.25
//        • 54.025, 28.1
//        • 53.73, 28.1
        return latitude < 54.25 && latitude > 53.73
                && longitude > 27.25 && longitude < 28.1;
    }

    public static double calculateDistanse(Location locationA,
                                           Location locationB) {
        float dis = locationA.distanceTo(locationB);
        double dis2 = gps2m(locationA.getLatitude(), locationA.getLongitude(),
                locationB.getLatitude(), locationB.getLongitude());
        return dis2 / 1000;
    }

    public static double calculateDistanse(Location location, double longitude,
                                           double lantitude) {
        if (longitude > 800 || lantitude > 800)
            return 90000;
        Location locationA = new Location("point A");
        locationA.setLatitude(lantitude);
        locationA.setLongitude(longitude);
        return calculateDistanse(locationA, location);
    }

    public static double calculateDistanse(double longitudeA,
                                           double lantitudeA, double longitudeB,
                                           double lantitudeB) {
        if (longitudeA > 800 || lantitudeA > 800 || longitudeB > 800 || lantitudeB > 800)
            return 90000;
        Location locationA = new Location("point A");
        Location locationB = new Location("point B");
        locationA.setLatitude(lantitudeA);
        locationA.setLongitude(longitudeA);
        locationB.setLatitude(lantitudeB);
        locationB.setLongitude(longitudeB);
        return calculateDistanse(locationA, locationB);
    }

    public static double gps2m(double lat_a, double lng_a, double lat_b,
                               double lng_b) {
        double pk = (180d / 3.141592654);
        double a1 = lat_a / pk;
        double a2 = lng_a / pk;
        double b1 = lat_b / pk;
        double b2 = lng_b / pk;

        double t1 = Math.cos(a1) * Math.cos(a2) * Math.cos(b1) * Math.cos(b2);
        double t2 = Math.cos(a1) * Math.sin(a2) * Math.cos(b1) * Math.sin(b2);
        double t3 = Math.sin(a1) * Math.sin(b1);
        double tt = Math.acos(t1 + t2 + t3);

        return 6366000 * tt;
    }

    public static boolean isNetworkConnected(Context context) {
        if (context == null)
            return false;
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            // There are no active networks.
            return false;
        } else
            return true;
    }



    public static String getDBFolderPath(Context context) {
        String folder = Environment.getDataDirectory().getAbsolutePath()
                + Environment.getDataDirectory().getAbsolutePath() + "/"
                + context.getPackageName() + "/databases/";
        return folder;
    }


    public static int getCurrentTime() {
        Calendar calendar = Calendar.getInstance();
        int minute = calendar.get(Calendar.HOUR_OF_DAY) * 60
                + calendar.get(Calendar.MINUTE);
        return minute;
    }

    public static int getCurrentDay() {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        if (day == 0)
            day = 7;
        return day;
    }




    public static float getMemory() {
        return (Runtime.getRuntime().maxMemory() / 1048576f);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static SharedPreferences getSharedPreferences(Context context,
                                                         String name) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            return context.getSharedPreferences(name,
                    Context.MODE_MULTI_PROCESS);
        return context.getSharedPreferences(name, Context.MODE_PRIVATE);
    }
}
