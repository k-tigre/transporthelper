package by.tigre.transtorthelper.core;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;


public class ZIP {

	public static boolean UnPackZIP(InputStream is, String baseURL) {
		ZipInputStream zis = null;
		FileOutputStream fout = null;
		try {
			String filename;
			zis = new ZipInputStream(new BufferedInputStream(is));
			
			ZipEntry ze;
			byte[] buffer = new byte[1024];
			int count;
			while ((ze = zis.getNextEntry()) != null) {
				filename = ze.getName();
				File fmd = new File(baseURL + "/" + filename);
				if (ze.isDirectory()) {
					fmd.mkdirs();
					continue;
				}
				fmd.getParentFile().mkdirs();
				fout = new FileOutputStream(baseURL + "/"
						+ filename);
				while ((count = zis.read(buffer)) != -1) {
					fout.write(buffer, 0, count);
				}
				fout.close();
				zis.closeEntry();
			}
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}finally{
			if (zis!=null)
				try {
					zis.close();
				} catch (Exception e2) {
				}
			if (fout!=null)
				try {
					fout.close();
				} catch (Exception e2) {
				}
		}
		return true;
	}

	public static boolean UnPackZIP(String fileZipString, String dirString) {
		boolean result = true;
		File zipFile = new File(fileZipString);
		File dir = new File(dirString);
		dir.mkdirs();
		if (zipFile.exists()) {
			FileInputStream is = null;
			try {
				is = new FileInputStream(zipFile);
				Log.i("UpdateService", "UnPackZIP " + zipFile.getName());
				if (UnPackZIP(is, dir.getAbsolutePath())) {
					Log.i("UpdateService", "UnPackZIP = true " + zipFile.getName());
					zipFile.delete();
					Log.i("UpdateService", "UnPackZIPdelete " + zipFile.getName());
					result = true;
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				result = false;
			} catch (Exception e) {
				e.printStackTrace();
				result = false;
			} finally {
				if (is != null) {
					try {
						is.close();
					} catch (Exception e2) {
					}
				}
			}
		} else {
			result = false;
		}
		return result;

	}

}