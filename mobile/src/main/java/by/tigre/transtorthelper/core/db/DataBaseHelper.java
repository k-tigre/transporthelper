package by.tigre.transtorthelper.core.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import by.tigre.transtorthelper.core.models.StopT;
import by.tigre.transtorthelper.core.models.TransportStop;


public class DataBaseHelper {
    private static final double dLa = 0.0045d; //~0.5km
    private static final double dLo = 0.0076284d; // ~0.5km 53

    public final static String DB_NAME = "TransBase.db";
    public static final int DATABASE_VERSION = 1;
    private static DataBaseHelper db;
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_LAT = "lat";
    private static final String COLUMN_LONG = "lng";

    private static final String LOG = "DB";
    private static DBHelper mDBHelper = null;
    private static SQLiteDatabase mDB;

    private DataBaseHelper() {
    }

    public static DataBaseHelper getInstance() {
        if (db == null) {
            db = new DataBaseHelper();
        }
        return db;
    }

    public SQLiteDatabase getmDB() {
        return mDB;
    }

    public boolean open(Context context) {
        if (mDBHelper == null)
            mDBHelper = new DBHelper(context, DB_NAME, null, DATABASE_VERSION);
        if (mDB == null)
            mDB = mDBHelper.getWritableDatabase();
        boolean result = false;
        try {
            Cursor cursor = mDB.query("settings", null, null, null, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    result = true;
                }
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
            try {
                mDB.close();
            } catch (Exception ignore) {
            }
            try {
                mDBHelper.close();
            } catch (Exception ignore) {
            }

            mDB = null;
            mDBHelper = null;
        }
        return result;
    }


//    public DBTransport(Context context) {
//        mDatabaseOpenHelper = new TransportOpenHelper(context);
//    }

    public static ArrayList<StopT> getAllTransport(Context context, Location location) {
        ArrayList<StopT> ts = null;
        try {
            String selection;
            String[] arg;
            if (location!=null){
                selection = "lat < ? AND lat > ? AND lng < ? AND lng > ?";
                arg = new String[]{String.valueOf(location.getLatitude() + dLa/2f),
                        String.valueOf(location.getLatitude() - dLa/2f),
                        String.valueOf(location.getLongitude() + dLo/2f),
                        String.valueOf(location.getLongitude() - dLo/2f)};
            }else{
                selection = null;
                arg = null;
            }
            Cursor cursor = context.getContentResolver().query(TransContentProvider.URI_STOPS, null, selection, arg, null);
            ts = new ArrayList<>(cursor.getCount());
            if (cursor.moveToFirst()) {
                do {
                    ts.add(new StopT(
                            cursor.getInt(cursor.getColumnIndex(COLUMN_ID)), cursor
                            .getString(cursor.getColumnIndex(COLUMN_NAME)),
                            cursor.getDouble(cursor.getColumnIndex(COLUMN_LAT)),
                            cursor.getDouble(cursor.getColumnIndex(COLUMN_LONG))
                    ));
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ts;
    }

    public static List<TransportStop> getTransportOnStop(Context context, int idStop) {
        List<TransportStop> transports = null;
        Log.e("DB", "stop = " + idStop);
        String table = "route_stops as stops INNER JOIN routes as route on stops.route_id = route.id";
        String select = "stop_id = " + idStop;
//        String[] columns = new String[]{"route_num", "transport_type_id",
//                "route_type_id", "start_time", "deltas", "name", "stop_index"};
        String[] columns = new String[]{"route_num", "transport_type_id",
                "route_type_id", "start_time", "name", "stop_index", "week_days", "week_days_details", "stop_id"};
//		 select * from routes
//		 where route_stops like '%14646%'
//		String table = "routes";
//		String select = "route_stops LIKE '%" + idStop+"%'";
//		String[] columns = new String[] { "route_num", "transport_type_id",
//				"route_type_id", "start_time", "deltas", "name", "route_stops" };
        try {

            transports = new ArrayList<>();
            Cursor cursor = context.getContentResolver().query(TransContentProvider.URI_TRANSPORT, columns, select, null, "route_type_id");
            if (cursor.moveToFirst()) {
                do {
                    TransportStop transport = new TransportStop(
                            cursor.getInt(cursor.getColumnIndex("transport_type_id")),
                            cursor.getString(cursor.getColumnIndex("start_time")),
                            cursor.getString(cursor.getColumnIndex("route_num")),
                            cursor.getString(cursor.getColumnIndex("name")),
                            cursor.getString(cursor.getColumnIndex("week_days")),
                            cursor.getString(cursor.getColumnIndex("week_days_details"))
                    );
                    transport.setStopIndex(cursor.getInt(cursor
                            .getColumnIndex("stop_index")));
                    transport.calcTime();
                    boolean add = true;
                    int remove = -1;
                    if (transport.getTime() < 0)
                        continue;
                    for (int i = 0; i < transports.size(); i++) {
                        if (transports.get(i).numberName.equals(transport.numberName)) {
                            add = false;
                            if (!transports.get(i).name.equals(transport.name)) {
                                if (transports.get(i).getTime() > transport.getTime()) {
                                    remove = i;
                                    add = true;
                                    break;
                                }
                            }
                        }
                    }
                    if (remove != -1) {
                        transports.remove(remove);
                    }
                    if (add)
                        transports.add(transport);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return transports;
    }


   /* public void open() throws SQLiteException {
        database = mDatabaseOpenHelper.getWritableDatabase();
    }

    public void close() {
        database.close();
    }*/

    private static class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory,
                        int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
//            db.execSQL(CREATE_TABLE_GROUP);
//            db.execSQL(CREATE_TABLE_RECIPE);
//            db.execSQL(CREATE_TABLE_INGREDIENT);
//            db.execSQL(TRIGGER_CLEAR_INGREDIENT);
//            db.execSQL(TRIGGER_TO_DELETE_INGREDIENT);
//            db.execSQL(TRIGGER_TO_DELETE_RECIPE);
//            db.execSQL(TRIGGER_CLEAR_RECIPE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }
}
