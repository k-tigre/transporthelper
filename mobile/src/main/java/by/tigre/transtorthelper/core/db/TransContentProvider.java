package by.tigre.transtorthelper.core.db;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentValues;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;

import java.util.ArrayList;

import by.tigre.transtorthelper.core.Constants;
import by.tigre.transtorthelper.core.DownLoaderHelper;
import by.tigre.transtorthelper.core.Utils;
import by.tigre.transtorthelper.service.SyncService;

public class TransContentProvider extends ContentProvider {
    public static final String BASE_URL_WEB_SWERVICE = "http://findforme.by";

    public final static String AUTHORITY = "by.tigre.transtorthelper.db";
    public static final String TABLE_STOPS = "stops";
    public static final Uri URI_STOPS = Uri.parse("content://" + AUTHORITY + "/" + TABLE_STOPS);
    public static final String TABLE_TRANSPORTS = "transports";
    public static final Uri URI_TRANSPORT = Uri.parse("content://" + AUTHORITY + "/" + TABLE_TRANSPORTS);
    public static final String SYNC = "sync";
    public static final Uri URI_SYNC = Uri.parse("content://" + AUTHORITY + "/" + SYNC);

    public static final int KEY_SYNC = 11;


    public static final int KEY_STOPS = 31;
    public static final int KEY_STOPS_ID = 32;

    public static final int KEY_TRANSPORTS = 41;
    public static final int KEY_TRANSPORTS_ID = 42;

    private static final UriMatcher uriMatcher;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, SYNC, KEY_SYNC);

        uriMatcher.addURI(AUTHORITY, TABLE_STOPS, KEY_STOPS);
        uriMatcher.addURI(AUTHORITY, TABLE_STOPS + "/#", KEY_STOPS_ID);

        uriMatcher.addURI(AUTHORITY, TABLE_TRANSPORTS, KEY_TRANSPORTS);
        uriMatcher.addURI(AUTHORITY, TABLE_TRANSPORTS + "/#", KEY_TRANSPORTS_ID);
    }

    private DataBaseHelper helper;

    @Override
    public boolean onCreate() {
        helper = DataBaseHelper.getInstance();
        helper.open(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String table = null;
        String groupBy = null;
        Cursor cursor = null;
        int key = uriMatcher.match(uri);
        if (!checkState()) {
            return null;
        }
        switch (key) {
            case KEY_STOPS_ID:
                break;
            case KEY_STOPS:
                table = TABLE_STOPS;
                break;
            case KEY_TRANSPORTS:
                table = "route_stops as stops INNER JOIN routes as route on stops.route_id = route.id";
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }
        if (table != null) {
            try {
                cursor = helper.getmDB().query(table, projection, selection, selectionArgs, groupBy, null, sortOrder);
                if (cursor != null) {
                    cursor.setNotificationUri(getContext().getContentResolver(), uri);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return cursor;
    }


    private boolean checkState() {
        boolean result = false;
        helper = DataBaseHelper.getInstance();
        result = helper.open(getContext());
        if (!result)
            sendErrorDataBaseState();
        return result;
    }

    private void sendErrorDataBaseState() {
        Intent intent = new Intent(Constants.ACTION_ERROR);
        intent.putExtra("type", "DATABASE");
        getContext().sendBroadcast(intent);
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        long id = -1;
        String table = null;
        Uri result = null;
        int key = uriMatcher.match(uri);
        if (key == KEY_SYNC) {
            id = updateDb() ? 1 : -1;
        } else {
            if (!checkState()) {
                return null;
            }
            switch (key) {
                case KEY_SYNC:
                    id = updateDb() ? 1 : -1;
                    break;
                default:
                    throw new IllegalArgumentException("Wrong URI: " + uri);
            }
            if (table != null) {
                id = helper.getmDB().insert(table, null, values);
            }
        }
        if (id > 0) {
//            getContext().getContentResolver().notifyChange(uri, null);
            result = Uri.withAppendedPath(uri, String.valueOf(id));
        }
        return result;
    }

    private boolean updateDb() {
        return DownLoaderHelper.downloadHttpTr(getContext(), "https://www.dropbox.com/s/0eds9h96wt12iuc/TransBase.zip?dl=1", Utils.getDBFolderPath(getContext()));
    }


    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        if (!checkState()) {
            return -1;
        }
        int count = 0;
        String table = null;
        if (table != null)
            count = helper.getmDB().delete(table, selection, selectionArgs);
        if (count > 0) {
//            getContext().getContentResolver().notifyChange(uri, null);
        }
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        if (!checkState()) {
            return -1;
        }
        int count = 0;
        String table = null;


        if (table != null)
            count = helper.getmDB().update(table, values, selection, selectionArgs);
        if (count > 0) {
//            getContext().getContentResolver().notifyChange(uri, null);
        }
        return 0;
    }

    @Override
    public ContentProviderResult[] applyBatch(@NonNull ArrayList<ContentProviderOperation> operations) throws OperationApplicationException {
        ContentProviderResult[] results = null;
        try {
            helper.getmDB().beginTransaction();
            results = super.applyBatch(operations);
            helper.getmDB().setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            helper.getmDB().endTransaction();
        }

        return results;
    }
}
