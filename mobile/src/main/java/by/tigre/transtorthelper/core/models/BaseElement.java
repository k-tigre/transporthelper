package by.tigre.transtorthelper.core.models;

public abstract class BaseElement {
	protected String name;
	protected  double latitude;
	protected  double longitude;

    protected BaseElement() {
    }

	public String getName() {
		return name;
	}
	
	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public BaseElement(String name, double lan, double lng) {
		this.name = name;
		this.latitude = lan;
		this.longitude = lng;
	}
	
}
