package by.tigre.transtorthelper.core.models;

import android.text.Html;
import android.util.Log;

import java.util.Calendar;

import by.tigre.transtorthelper.core.Utils;


public class TransportStop implements Comparable<TransportStop> {


    public enum TypeTransport {
        BUS, TROLL, TRAM, METRO;

        public static TypeTransport getValueOf(int id) {
            switch (id) {
                case 1:
                    return BUS;
                case 2:
                    return METRO;
                case 3:
                    return TRAM;
                case 4:
                    return TROLL;
                default:
                    return BUS;
            }
        }
    }

    public final TypeTransport type;
    public final String startTime;
    public final String numberName;
    public final String name;
    private final String weekDays, weekDaysDetails;
    private int time;

    public String getTimeFormated(String formatM, String formatH) {
        if (time > 60)
            return String.format(formatH, time / 60, time % 60);

        return String.format(formatM, time);
    }

    public int getTime() {
        return time;
    }

    private int stopIndex;

    public int getStopIndex() {
        return stopIndex;
    }

    public void setStopIndex(int stopIndex) {
        this.stopIndex = stopIndex;
    }

    public TransportStop(int type, String startTime, String numberName, String name, String weekDays, String weekDaysDetails) {
        this.type = TypeTransport.getValueOf(type);
        this.startTime = startTime;
        this.numberName = Html.fromHtml(numberName).toString();
        this.name = Html.fromHtml(name).toString();
        this.weekDays = weekDays;
        this.weekDaysDetails = weekDaysDetails;
    }

    public void calcTime() {
        int minuteCurrent = Utils.getCurrentTime();
        Calendar calendar = Calendar.getInstance();
        int currentDay = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        if (currentDay == 0)
            currentDay = 7;
        if (!weekDays.contains(String.valueOf(currentDay))) {
            time = -1;
            return;
        }

        int deltaTimeYesterday = 0;
        if (minuteCurrent < 180) {
            if (currentDay - 1 == 0) {
                deltaTimeYesterday = nextStopTime(7, minuteCurrent + 24 * 60);
            } else {
                deltaTimeYesterday = nextStopTime(currentDay - 1, minuteCurrent + 24 * 60);
            }
        }

        int deltaTimeToday = nextStopTime(currentDay, minuteCurrent);
        time = ((deltaTimeToday > deltaTimeYesterday) && (deltaTimeYesterday != 0)) ? deltaTimeYesterday : deltaTimeToday;
    }

    private int nextStopTime(int currentDay, int minuteCurrent) {
//    	Вывод всего рассписания автобуса на текущий день
//    	checkStopToday(currentDay);
        String[] weekDayDetailsArray = weekDaysDetails.split(",");
        String[] startTimeArray = startTime.split(",");
        int delta = 0;
        try {
            for (int i = (stopIndex - 1) * weekDayDetailsArray.length; i < stopIndex * weekDayDetailsArray.length; i++) {
                if (weekDayDetailsArray[i - (stopIndex - 1) * weekDayDetailsArray.length].contains(String.valueOf(currentDay)) &&
                        Integer.valueOf(startTimeArray[i]) > minuteCurrent) {
                    delta = Integer.valueOf(startTimeArray[i]);
                    break;
                }
            }
        } catch (Exception e) {
        }
        Log.i("time", numberName + " " + name + " time " + delta / 60 + ":" + delta % 60);
        return delta - minuteCurrent;
    }

    private void checkStopToday(int currentDay) {
        Log.i("time", "Рассписание " + numberName + ": ");
        String[] weekDayDetailsArray = weekDaysDetails.split(",");
        String[] startTimeArray = startTime.split(",");
        String timeStop = " ";
        for (int i = (stopIndex - 1) * weekDayDetailsArray.length; i < stopIndex * weekDayDetailsArray.length; i++) {
            try {
                if (weekDayDetailsArray[i - (stopIndex - 1) * weekDayDetailsArray.length].contains(String.valueOf(currentDay))) {
                    timeStop += " " + Integer.valueOf(startTimeArray[i]) / 60 + ":" + Integer.valueOf(startTimeArray[i]) % 60;
                }
            } catch (Exception e) {
            }
        }
        Log.i("time", timeStop);
    }

    @Override
    public int compareTo(TransportStop another) {
        return time - another.time;
    }

}
