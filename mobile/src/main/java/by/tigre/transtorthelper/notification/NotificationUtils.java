package by.tigre.transtorthelper.notification;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;

import by.tigre.transtorthelper.R;
import by.tigre.transtorthelper.service.LocationService;
import by.tigre.transtorthelper.ui.MainActivity;

public class NotificationUtils {

    public static Notification getNotification(Context context, String title, String message, boolean autoCancel,
                                               Intent intent) {
        NotificationCompat.Builder nb = new NotificationCompat.Builder(context);
        nb.setContentTitle(title);
        if (!TextUtils.isEmpty(message)) {
            nb.setContentText(message);
        }
        nb.setSmallIcon(android.R.drawable.ic_menu_mylocation);
        if (intent==null) {
            intent = new Intent(context, MainActivity.class);
            intent.setAction("NOTIFICATION_LOCATION");
        }
        intent
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        Uri data1 = Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME));
        intent.setData(data1);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 998, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        nb.setContentIntent(pendingIntent);
        nb.setOnlyAlertOnce(true);
        nb.setDefaults(0);

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
        return nb.build();
//        } else {
//            return nb.getNotification();
//        }
    }

    public static Notification getNotification(Context context, String title, String message, boolean autoCancel) {
        return getNotification(context, title, message, autoCancel, null);
    }
}