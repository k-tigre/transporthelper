package by.tigre.transtorthelper.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import by.tigre.transtorthelper.service.LocationService;

public class StateReceiver extends BroadcastReceiver {
    public StateReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.i("StateReceiver", "action = " + action);
        LocationService.UpdateLocationTransport(context);
    }
}
