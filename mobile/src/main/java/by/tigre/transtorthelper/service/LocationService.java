package by.tigre.transtorthelper.service;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import by.tigre.transtorthelper.core.Constants;
import by.tigre.transtorthelper.core.Utils;
import by.tigre.transtorthelper.core.db.DataBaseHelper;
import by.tigre.transtorthelper.core.models.StopT;
import by.tigre.transtorthelper.core.models.TransportStop;
import by.tigre.transtorthelper.notification.NotificationUtils;
import by.tigre.transtorthelper.ui.StopsActivity;

public class LocationService extends Service implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {


    public static void UpdateLocationTransport(Context context) {
        Intent intent = new Intent(context, LocationService.class);
        intent.setAction(ACTION_START_LOCATION);
        context.startService(intent);
    }

    public static void StartLocationService(Context context) {
        Intent intent = new Intent(context, LocationService.class);
        intent.setAction(ACTION_START_LOCATION);
        context.startService(intent);
    }

    public static void StopLocationService(Context context) {
        Intent intent = new Intent(context, LocationService.class);
        intent.setAction(ACTION_STOP_LOCATION);
        context.startService(intent);
    }

    public static final String ACTION_START_LOCATION = "ServiceLocation.start_location";
    public static final String ACTION_STOP_LOCATION = "ServiceLocation.stop_location";
    public static final String ACTION_UPDATE_LOCATION = "ServiceLocation.update_location";
    public static final String ACTION_UPDATE_LOCATION_STATUS = "ServiceLocation.update_location_status";

    private static final String TAG = "ServiceLocation";
    private final Object lock = new Object();
    private GoogleApiClient mGoogleApiClient;
    private Location location;
    private Notification notification;
    private StateReceiver receiver;

    class StateReceiver extends BroadcastReceiver {
        public StateReceiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.i("StateReceiverService", "action = " + action);
            LocationService.UpdateLocationTransport(context);
        }
    }


    @Override
    public void onCreate() {
        super.onCreate();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        receiver = new StateReceiver();

        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_USER_PRESENT);
        filter.addAction(Intent.ACTION_BOOT_COMPLETED);
        registerReceiver(receiver, filter);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String action = intent != null ? intent.getAction() : "";
        switch (action) {
            case ACTION_START_LOCATION:
                startLocation();
                break;
            case ACTION_STOP_LOCATION:
                stopLocation();
                break;
            case ACTION_UPDATE_LOCATION:
                updateLocation();
                break;
            case ACTION_UPDATE_LOCATION_STATUS:
                sendLocationStatus();
                break;
        }
        Log.i(TAG, "action=" + action);
        return START_REDELIVER_INTENT;
    }

    private void updateLocationStatus() {

    }

    private void updateLocation() {
        if (location != null) {

            ArrayList<StopT> ts = DataBaseHelper.getAllTransport(this, location);
            Log.i(TAG, "ts = " + ts);
            Toast.makeText(this, "Найдено " + (ts != null ? ts.size() : 0) + " остановок поблизости", Toast.LENGTH_SHORT).show();
            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            Intent intent = new Intent(this, StopsActivity.class);
            intent.setAction(Constants.ACTION_TRANSTORT);
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("stops", ts);
            bundle.putParcelable("location", location);
            intent.putExtras(bundle);


// mId allows you to update the notification later on.
            mNotificationManager.notify("notify",19621, NotificationUtils.getNotification(this, "Найдено остановок: ",
                    "" + (ts != null ? ts.size() : 0), true, intent));
//            Intent intent = new Intent(this, LocationService.class);
//            intent.putExtra("location", location);
//            startService(intent);
        }
    }

    private void startAlarm() {
        Log.i(TAG, "addAlarm");
        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        Intent intent = new Intent(this, LocationService.class);
        intent.setAction(ACTION_UPDATE_LOCATION);
        PendingIntent sender = PendingIntent.getService(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        try {
            am.cancel(sender);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            am.setInexactRepeating(AlarmManager.RTC_WAKEUP, 0, 15 * 1000L, sender);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopAlarm() {
        Log.i(TAG, "removeAlarm");
        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, LocationService.class);
        intent.setAction(ACTION_UPDATE_LOCATION);
        PendingIntent sender = PendingIntent.getService(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        try {
            am.cancel(sender);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startLocation() {
//        startAlarm();
        if (!mGoogleApiClient.isConnected() && !mGoogleApiClient.isConnecting()) {
            mGoogleApiClient.connect();
            sendLocationStatus();
            notification = NotificationUtils.getNotification(this, "Соединение...", null, true);
            startForeground(19091, notification);

        } else if (mGoogleApiClient.isConnected()) {
            sendLocationStatus();
            sendLocation(LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient));
        }
        Log.i(TAG, "mGoogleApiClient isConnected " + mGoogleApiClient.isConnected() + "; isConnecting-" + mGoogleApiClient.isConnecting());
    }

    private void sendLocation(Location location) {
        synchronized (lock) {
            this.location = location;
            updateLocation();
        }

        Intent intent = new Intent(ACTION_UPDATE_LOCATION);
        if (location != null) {
            notification = NotificationUtils.getNotification(this, "Положение установлено", Utils.TIME_DATE_FORMAT.format(location.getTime()), true);

            startForeground(19091, notification);
            intent.putExtra("location", location);
            intent.putExtra("hasLocation", true);
        } else {
            intent.putExtra("hasLocation", false);
            notification = NotificationUtils.getNotification(this, "Положение не установлено", Utils.TIME_DATE_FORMAT.format(System.currentTimeMillis()), true);
            startForeground(19091, notification);
        }
        sendBroadcast(intent);
    }

    private void sendLocationStatus() {
        Intent intent = new Intent(ACTION_UPDATE_LOCATION_STATUS);
        intent.putExtra("isConnecting", mGoogleApiClient.isConnecting());
        intent.putExtra("isConnected", mGoogleApiClient.isConnected());
        sendBroadcast(intent);
    }

    private void stopLocation() {
//        stopAlarm();
        mGoogleApiClient.disconnect();
        sendLocationStatus();
        stopSelf();
    }

    @Override
    public void onDestroy() {
        mGoogleApiClient.disconnect();
        unregisterReceiver(receiver);
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onConnected(Bundle bundle) {
        sendLocationStatus();
        LocationRequest mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
        mLocationRequest.setInterval(60 * 1000); // Update location every second
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setSmallestDisplacement(50);

        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
        sendLocation(LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient));
    }

    @Override
    public void onConnectionSuspended(int i) {
        sendLocationStatus();
        Log.i(TAG, "GoogleApiClient connection has been suspend");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        sendLocationStatus();
        Log.i(TAG, "GoogleApiClient connection has failed");
    }

    @Override
    public void onLocationChanged(Location location) {
        sendLocation(location);
        Log.i(TAG, "onLocationChanged " + location.toString());
    }
}
