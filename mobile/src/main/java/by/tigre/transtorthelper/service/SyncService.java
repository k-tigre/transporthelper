package by.tigre.transtorthelper.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import by.tigre.transtorthelper.core.Constants;
import by.tigre.transtorthelper.core.Constants.*;
import by.tigre.transtorthelper.core.db.TransContentProvider;

import static by.tigre.transtorthelper.core.Constants.ACTION_CMD_STATE;
import static by.tigre.transtorthelper.core.Constants.*;

public class SyncService extends Service {


    public SyncService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.getAction() != null) {
            switch (intent.getAction()) {
                case ACTION_CMD_STATE:
                    checkState();
                    break;
                case ACTION_CMD_SYNC:
                    syncTransport(intent.getBooleanExtra(KEY_FORCE, false));
                    break;
            }
        }
        return START_REDELIVER_INTENT;
    }

    private void checkState() {
        if (async == null) {
            sendCallbackBroadcast(SyncService.this, false, 1);
        } else {
            sendCallbackBroadcast(SyncService.this, false, 0);
        }
    }

    private static void sendCallbackBroadcast(Context context, boolean error, int progress) {
        Log.i("TEST", "async = " + async);
        Intent intent = new Intent(!error ? ACTION_STATE : ACTION_ERROR);
//        intent.putExtra("KEY_ERROR", error);
        intent.putExtra(KEY_PROGRESS, progress);
        context.sendBroadcast(intent);
    }

    private static Async async;

    private void syncTransport(boolean force) {
        long last = 0;
        if (!force) {
            SharedPreferences sp = getSharedPreferences(KEY_SETTINGS, MODE_PRIVATE);
            last = sp.getLong(KEY_LAST_UPDATE, 0);
        }
        Log.i("TEST", "last = " + last + "; async = " + async);
        if (System.currentTimeMillis() - last > 1000 * 60 * 60 * 24) {
            if (async == null) {
                async = new Async();
                async.execute();
            } else {
                sendCallbackBroadcast(SyncService.this, false, 0);
            }
        }
    }

    private class Async extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            sendCallbackBroadcast(SyncService.this, false, 0);
        }

        @Override
        protected void onPostExecute(Boolean error) {
            super.onPostExecute(error);

            async = null;
            if (!error) {
                SharedPreferences sp = getSharedPreferences(KEY_SETTINGS, MODE_PRIVATE);
                sp.edit().putLong(KEY_LAST_UPDATE, System.currentTimeMillis()).apply();
            }
            sendCallbackBroadcast(SyncService.this, error, 1);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            return getContentResolver().insert(TransContentProvider.URI_SYNC, null) == null;
        }
    }
}
