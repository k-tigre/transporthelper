package by.tigre.transtorthelper.ui;

import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import by.tigre.transtorthelper.R;
import by.tigre.transtorthelper.core.db.TransContentProvider;
import by.tigre.transtorthelper.service.LocationService;
import by.tigre.transtorthelper.ui.base.BaseActivity;

/**
 * Created by tigre on 07.03.15.
 */
public class MainActivity extends BaseActivity implements View.OnClickListener {
    private View progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.button).setOnClickListener(this);
        findViewById(R.id.button_start).setOnClickListener(this);
        findViewById(R.id.button_stop).setOnClickListener(this);
        progress = findViewById(R.id.progressBar);
        sentActionSync(false);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button:
                Cursor cursor = getContentResolver().query(TransContentProvider.URI_TRANSPORT, null, null, null, null);
                if (cursor != null) {
                    Log.i("TEST", "c = " + cursor.getCount());
                    cursor.close();
                    showProgress(false);
                } else {
                    Log.i("TEST", "c = " + null);
                }
                break;
            case R.id.button_start:
                LocationService.StartLocationService(this);
                break;
            case R.id.button_stop:
                LocationService.StopLocationService(this);
                break;
        }

    }

    @Override
    protected void showProgress(boolean show) {
        progress.setVisibility(show ? View.VISIBLE : View.GONE);
    }
}
