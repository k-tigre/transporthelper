package by.tigre.transtorthelper.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import by.tigre.transtorthelper.R;
import by.tigre.transtorthelper.core.db.DataBaseHelper;
import by.tigre.transtorthelper.core.models.StopT;
import by.tigre.transtorthelper.core.models.TransportStop;
import by.tigre.transtorthelper.ui.base.BaseActivity;

/**
 * Created by tigre on 07.03.15.
 */
public class StopsActivity extends BaseActivity {
    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stops);
        text = (TextView) findViewById(R.id.text_view);
        onHandleIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    private void onHandleIntent(Intent intent) {
        if (intent==null)
            return;

        ArrayList<StopT> ts = intent.getParcelableArrayListExtra("stops");
        intent.getParcelableExtra("location");
        if (ts != null) {
            text.setText("");
            for (StopT stopT : ts) {
                List<TransportStop> transportStop = DataBaseHelper.getTransportOnStop(this, stopT.id);
                Collections.sort(transportStop);
                StringBuilder tram = new StringBuilder();
                StringBuilder bus = new StringBuilder();
                StringBuilder troll = new StringBuilder();
                StringBuilder metro = new StringBuilder();
                String formateM = "через %d мин ";
                String formateH = "через %d ч %d мин ";
                for (TransportStop transport : transportStop) {
                    if (transport.getTime() < 0)
                        continue;
                    switch (transport.type) {
                        case BUS:
                            if (bus.length() > 0) {
                                bus.append("\n");
                            }
                            bus.append(transport.numberName);
                            bus.append(" - ");
                            bus.append(transport.getTimeFormated(formateM, formateH));
                            bus.append(" (");
                            bus.append(transport.name);
                            bus.append(")");
                            break;
                        case TRAM:
                            if (tram.length() > 0) {
                                tram.append("\n");
                            }
                            tram.append(transport.numberName);
                            tram.append(" - ");
                            tram.append(transport.getTimeFormated(formateM, formateH));
                            tram.append(" (");
                            tram.append(transport.name);
                            tram.append(")");
                            break;
                        case METRO:
                            if (metro.length() > 0) {
                                metro.append("\n");
                            }
                            metro.append(transport.numberName);
                            metro.append(" - ");
                            metro.append(transport.getTimeFormated(formateM, formateH));
                            metro.append(" (");
                            metro.append(transport.name);
                            metro.append(")");
                            break;
                        case TROLL:
                            if (troll.length() > 0) {
                                troll.append("\n");
                            }
                            troll.append(transport.numberName);
                            troll.append(" - ");
                            troll.append(transport.getTimeFormated(formateM, formateH));
                            troll.append(" (");
                            troll.append(transport.name);
                            troll.append(")");
                            break;
                    }
                }
                Log.i(getClass().getName(), "автобус: " + bus.toString());
                Log.i(getClass().getName(), "трамвай: " + tram.toString());
                Log.i(getClass().getName(), "троллейбус: " + troll.toString());
                Log.i(getClass().getName(), "метро: " + metro.toString());
                text.append(
                        "Остановка: " + stopT.getName() + "\n" +
                                "автобус: " + bus.toString() + "\n" +
                                "трамвай: " + tram.toString() + "\n" +
                                "троллейбус: " + troll.toString() + "\n" +
                                "метро: " + metro.toString() + "\n"
                        +"---------------------\n"
                );
            }
        }
    }

    @Override
    protected void onShowTransport(String name, long id) {
        super.onShowTransport(name, id);

    }

    @Override
    protected void showProgress(boolean show) {

    }
}
