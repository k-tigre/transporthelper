package by.tigre.transtorthelper.ui.base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;

import com.afollestad.materialdialogs.MaterialDialog;

import by.tigre.transtorthelper.service.SyncService;

import static by.tigre.transtorthelper.core.Constants.*;

/**
 * Created by tigre on 07.03.15.
 */
public abstract class BaseActivity extends ActionBarActivity {
    private BroadcastReceiver receiverSync = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent!=null && intent.getAction()!=null){
                switch (intent.getAction()){
                    case ACTION_ERROR:
                        showDataBaseError(intent.getStringExtra("type"));
                        break;
                    case ACTION_STATE:
                        Log.i("TEST","state = "+ intent.getIntExtra(KEY_PROGRESS, -1));
                        showProgress(intent.getIntExtra(KEY_PROGRESS, -1) == 0);
                        break;
//                    case ACTION_TRANSTORT:
//                        showProgress(intent.getIntExtra("key_id_stop", -1) == 0);
//                        break;
                }
            }
        }
    };

    protected void showDataBaseError(String text){
        new MaterialDialog.Builder(this).callback(new MaterialDialog.ButtonCallback() {
            @Override
            public void onNegative(MaterialDialog dialog) {
                super.onNegative(dialog);
                finish();
            }

            @Override
            public void onPositive(MaterialDialog dialog) {
                super.onPositive(dialog);
                sentActionSync(true);

            }
        })
                .title("Ошибка базы")
                .content(text!=null ? text: "Попробовать загрузить базу еще раз?")
                .positiveText("Загрузить")
                .negativeText("Отмена")
        .show();
    }

    protected void sentActionSync(boolean force){
        Intent intent = new Intent(this, SyncService.class);
        intent.setAction(ACTION_CMD_SYNC);
        intent.putExtra("key_force", force);
        startService(intent);
    }
    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_ERROR);
        filter.addAction(ACTION_STATE);
//        filter.addAction(ACTION_TRANSTORT);
        registerReceiver(receiverSync, filter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(receiverSync);
    }

    protected abstract void showProgress(boolean show);
    protected void onShowTransport(String name, long id){};
}
